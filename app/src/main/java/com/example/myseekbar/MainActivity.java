package com.example.myseekbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textView = findViewById(R.id.seek_bar_progress);
        final TextView textView1 = findViewById(R.id.seek_bar_temp_progress);

        GearSeekBarParent seekBar = findViewById(R.id.seek_bar);
        seekBar.setListener(level -> textView.setText(String.valueOf(level)));
        seekBar.setTempListener(level -> textView1.setText(String.valueOf(level)));
    }
}