package com.example.myseekbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;

/**
 * 自定义弧形SeekBar 弧线部分
 * Created by Jaceli on 2016-04-28.
 */
public class SeekBarArcView extends View {

    private Paint paint;
    private Path path;

    //   1    3    5
    //   |    |    |
    //   7----0----8
    //   |    |    |
    //   2    4    6
    //78两点只画图使用，0-6同时还代表档位
    private PointF pointFt1;
    private PointF pointFt2;
    private PointF pointFt3;
    private PointF pointFt4;
    private PointF pointFt5;
    private PointF pointFt6;
    private PointF pointFt7;
    private PointF pointFt8;
    private PointF pointFt0;

    private final int arcColor;         // 弧的颜色
    private final int arcWidth;         // 弧的宽度

    public SeekBarArcView(Context context, int arcColor, int arcWidth) {
        super(context);
        this.arcColor = arcColor;
        this.arcWidth = arcWidth;
        init();
    }

    public SeekBarArcView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.arcColor = Color.BLACK;
        this.arcWidth = 90;
        init();
    }

    private void init() {
        paint = new Paint();
        path = new Path();
        pointFt1 = new PointF();
        pointFt2 = new PointF();
        pointFt3 = new PointF();
        pointFt4 = new PointF();
        pointFt5 = new PointF();
        pointFt6 = new PointF();
        pointFt7 = new PointF();
        pointFt8 = new PointF();
        pointFt0 = new PointF();
        // 初始化画笔
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(arcColor);
        paint.setStrokeWidth(arcWidth);
        paint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left - 10, top, right + 10, bottom);
        // 此处设置0-8点坐标，这个坐标边距是固定的30，最好完善重比例
        pointFt1.set(30, 30);
        pointFt2.set(30, bottom - top - 30);
        pointFt3.set((float) (right - left) / 2, 30);
        pointFt4.set((float) (right - left) / 2, bottom - top - 30);
        pointFt5.set(right - left - 30, 30);
        pointFt6.set(right - left - 30, bottom - top - 30);
        pointFt7.set(30, (float) (bottom - top) / 2);
        pointFt8.set(right - left - 30, (float) (bottom - top) / 2);
        pointFt0.set((float) (right - left) / 2, (float) (bottom - top) / 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 画档位移动线
        path.moveTo(pointFt1.x, pointFt1.y);
        path.lineTo(pointFt2.x, pointFt2.y);
        path.moveTo(pointFt3.x, pointFt3.y);
        path.lineTo(pointFt4.x, pointFt4.y);
        path.moveTo(pointFt5.x, pointFt5.y);
        path.lineTo(pointFt6.x, pointFt6.y);
        path.moveTo(pointFt7.x, pointFt7.y);
        path.lineTo(pointFt8.x, pointFt8.y);
        canvas.drawPath(path, paint);
    }

    // 为了给后面使用的类不再重新计算点，引出get方法
    public PointF getPointFt1() {
        return pointFt1;
    }

    public PointF getPointFt2() {
        return pointFt2;
    }

    public PointF getPointFt3() {
        return pointFt3;
    }

    public PointF getPointFt4() {
        return pointFt4;
    }

    public PointF getPointFt5() {
        return pointFt5;
    }

    public PointF getPointFt6() {
        return pointFt6;
    }

    public PointF getPointFt7() {
        return pointFt7;
    }

    public PointF getPointFt8() {
        return pointFt8;
    }

    public PointF getPointFt0() {
        return pointFt0;
    }
}
